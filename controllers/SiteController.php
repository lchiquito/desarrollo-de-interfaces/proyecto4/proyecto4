<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Equipo;
use app\models\Ciclista;
use app\models\Puerto;
use app\models\Etapa;
use app\models\Maillot;
use app\models\Lleva;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEquipo() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nomequipo, COUNT(dorsal)nciclistas')->distinct()->groupBy('nomequipo'),
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);
        $variable = yii:: $app->db->createCommand("SELECT DISTINCT c1.nomequipo,COUNT(numetapa)etapasGanadas FROM etapa RIGHT JOIN (select nomequipo,dorsal FROM ciclista )c1 USING(dorsal)GROUP BY c1.nomequipo")->queryAll();
        $variablepuerto = yii:: $app->db->createCommand("SELECT DISTINCT c1.nomequipo,COUNT(nompuerto)puertosganados FROM puerto RIGHT JOIN (select nomequipo,dorsal FROM ciclista )c1 USING(dorsal)GROUP BY c1.nomequipo")->queryAll();
        $variablemaillot = yii:: $app->db->createCommand("SELECT DISTINCT c1.nomequipo,COUNT(código)maillotsllevados FROM lleva RIGHT JOIN (select nomequipo,dorsal FROM ciclista )c1 USING(dorsal)GROUP BY c1.nomequipo")->queryAll();

        return $this->render("equipo", [
                    "resultado" => $dataProvider,
                    "variable" => $variable,
                    "variablepuerto" => $variablepuerto,
                    "variablemaillot" => $variablemaillot,
        ]);
    }

    public function actionCiclistasequipo($nomequipo) {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nombre, edad, dorsal,nomequipo')->distinct()->where("nomequipo='$nomequipo'"),
        ]);
        $variable = yii:: $app->db->createCommand("SELECT DISTINCT c1.nombre,COUNT(numetapa)etapasGanadas FROM etapa RIGHT JOIN (select nombre,dorsal FROM ciclista WHERE nomequipo='$nomequipo')c1 USING(dorsal)GROUP BY c1.nombre")->queryAll();
        $variablepuerto = yii:: $app->db->createCommand("SELECT DISTINCT c1.nombre,COUNT(nompuerto)puertosganados FROM puerto RIGHT JOIN (select nombre,dorsal FROM ciclista WHERE nomequipo='$nomequipo')c1 USING(dorsal)GROUP BY c1.nombre")->queryAll();
        $variablemaillot = yii:: $app->db->createCommand("SELECT DISTINCT c1.nombre,COUNT(código)maillotsllevados FROM lleva RIGHT JOIN (select nombre,dorsal FROM ciclista WHERE nomequipo='$nomequipo')c1 USING(dorsal)GROUP BY c1.nombre")->queryAll();
        return $this->render("ciclistasequipo", [
                    "resultado" => $dataProvider,
                    "nomequipo" => $nomequipo,
                    "variable" => $variable,
                    "variablepuerto" => $variablepuerto,
                    "variablemaillot" => $variablemaillot,
        ]);
    }

    public function actionEstadisticaequipo($nomequipo) {
        $eganadasequipo = new ActiveDataProvider([
            'query' => Etapa::find()->select('COUNT(numetapa)eganadas')->distinct()->where("dorsal IN (select dorsal FROM ciclista WHERE nomequipo='$nomequipo')"),
        ]);
        $c1 = (new \yii\db\Query())->select('etapa.dorsal,COUNT(numetapa)etapasGanadas,nomequipo')->distinct()->from("etapa")->innerJoin('ciclista', 'etapa.dorsal = ciclista.dorsal')->groupBy("dorsal");
        $c2 = (new \yii\db\Query())->select('dorsal,MAX(etapasGanadas)etapasGanadas')->distinct()->from($c1)->groupBy("nomequipo");
        $mejorciclistaequipo = (new \yii\db\Query())->select('ciclista.dorsal,nomequipo,edad, nombre, etapasGanadas')->distinct()->from("ciclista")->innerJoin(['c2' => $c2], 'ciclista.dorsal = c2.dorsal')->where("nomequipo='$nomequipo'")->all();

        $puertosganados = (new \yii\db\Query())->select('COUNT(nompuerto)puertosganados')->distinct()->from('puerto')->where("dorsal IN (select dorsal FROM ciclista WHERE nomequipo='$nomequipo')")->scalar();
        $maillitsganados = (new \yii\db\Query())->select('COUNT(código)maillotsllevados')->distinct()->from('lleva')->where("dorsal IN (select dorsal FROM ciclista WHERE nomequipo='$nomequipo')")->scalar();
        $subconsultaranking2 = (new \yii\db\Query())->select("numetapa,nomequipo,ciclista.dorsal")->FROM("ciclista")
                ->innerJoin('etapa', 'ciclista.dorsal = etapa.dorsal');
        $subconsultaranking = (new \yii\db\Query())->select("nomequipo,COUNT(numetapa)etapasganadas")->from(['ciclista' => $subconsultaranking2])->groupby("nomequipo");
        $rankingequipo = (new \yii\db\Query())->SELECT("COUNT(*)+1")->FROM($subconsultaranking)
                        ->WHERE("etapasganadas>(SELECT COUNT(numetapa)FROM etapa WHERE etapa.dorsal IN (select ciclista.dorsal FROM ciclista WHERE nomequipo='$nomequipo'))")->scalar();
        $subconsultaranking2puerto = (new \yii\db\Query())->select("nompuerto,nomequipo,ciclista.dorsal")->FROM("ciclista")
                ->innerJoin('puerto', 'ciclista.dorsal = puerto.dorsal');
        $subconsultarankingpuerto = (new \yii\db\Query())->select("nomequipo,COUNT(nompuerto)puertosganados")->from(['ciclista' => $subconsultaranking2puerto])->groupby("nomequipo");

        $rankingequipopuerto = (new \yii\db\Query())->SELECT("COUNT(*)+1")->FROM($subconsultarankingpuerto)
                        ->WHERE("puertosganados>(SELECT COUNT(nompuerto)FROM puerto WHERE dorsal IN (select ciclista.dorsal FROM ciclista WHERE nomequipo='$nomequipo'))")->scalar();
        $subconsultaranking2maillots = (new \yii\db\Query())->select("código,nomequipo,ciclista.dorsal")->FROM("ciclista")
                ->innerJoin('lleva', 'ciclista.dorsal = lleva.dorsal');
        $subconsultarankingmaillots = (new \yii\db\Query())->select("nomequipo,COUNT(código)maillotsllevados")->from(['ciclista' => $subconsultaranking2maillots])->groupby("nomequipo");

        $rankingequipomaillots = (new \yii\db\Query())->SELECT("COUNT(*)+1")->FROM($subconsultarankingmaillots)
                        ->WHERE("maillotsllevados>(SELECT COUNT(código)FROM lleva WHERE dorsal IN (select ciclista.dorsal FROM ciclista WHERE nomequipo='$nomequipo'))")->scalar();
        $edadmedia = Ciclista::find()->select("AVG(edad)")->where("nomequipo='$nomequipo'")->scalar();
        $nombredirector = Equipo::find()->select('director')->distinct()->where("nomequipo='$nomequipo'")->scalar();

        return $this->render("estadisticaequipo", [
                    "puertosganados" => $puertosganados,
                    "rankingequipo" => $rankingequipo,
                    "rankingequipopuerto" => $rankingequipopuerto,
                    "edadmedia" => round($edadmedia),
                    "nombredirector" => $nombredirector,
                    "nomequipo" => $nomequipo,
                    "eganadasequipo" => $eganadasequipo,
                    "maillitsganados" => $maillitsganados,
                    "rankingequipomaillots" => $rankingequipomaillots,
                    "mejorciclistaequipo" => $mejorciclistaequipo,
        ]);
    }

    public function actionDatosciclista($dorsal) {
        $datosciclista = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nombre, edad, dorsal,nomequipo')->distinct()->where("dorsal='$dorsal'"),
        ]);
        $maillotsllevados = (new \yii\db\Query())->select('COUNT(código)maillotsllevados')->distinct()->from('lleva')->where("dorsal='$dorsal'")->scalar();
        $etapasganadasciclista = Etapa::find()->select('COUNT(numetapa)egciclista')->where("dorsal='$dorsal'")->scalar();
        $puertosganadasciclista = Puerto::find()->select('COUNT(nompuerto)pganados')->where("dorsal='$dorsal'")->scalar();
        $datospuertos = Puerto::find()->select('nompuerto, altura, pendiente')->where("dorsal='$dorsal'")->all();
        $datosetapa = Etapa::find()->select('numetapa, kms, salida, llegada')->where("dorsal='$dorsal'")->all();
        $datosmaillot = Maillot::find()->select("premio, color, tipo, lleva.código AS codigo1, lleva.numetapa AS numetapa1")
                        ->from('maillot')->leftJoin('lleva', 'lleva.código = maillot.código')->groupby("dorsal,numetapa")->having("lleva.dorsal='$dorsal'")->all();

        return $this->render("datosciclista", [
                    "etapasganadasciclista" => $etapasganadasciclista,
                    "datosciclista" => $datosciclista,
                    "puertosganadasciclista" => $puertosganadasciclista,
                    "datospuertos" => $datospuertos,
                    "datosetapa" => $datosetapa,
                    "datosmaillot" => $datosmaillot,
                    "maillotsllevados" => $maillotsllevados,
        ]);
    }

    public function actionDatosciclistaindividual() {
        $datosciclista = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nombre, edad, dorsal,nomequipo')->distinct(),
            'pagination' => [
                'pageSize' => 110,
            ],
        ]);
        $variable = yii:: $app->db->createCommand("SELECT DISTINCT c1.nombre,COUNT(numetapa)etapasGanadas FROM etapa RIGHT JOIN (select nombre,dorsal FROM ciclista )c1 USING(dorsal)GROUP BY c1.nombre")->queryAll();
        $variablepuerto = yii:: $app->db->createCommand("SELECT DISTINCT c1.nombre,COUNT(nompuerto)puertosganados FROM puerto RIGHT JOIN (select nombre,dorsal FROM ciclista )c1 USING(dorsal)GROUP BY c1.nombre")->queryAll();
        $variablemaillot = yii:: $app->db->createCommand("SELECT DISTINCT c1.nombre,COUNT(código)maillotsllevados FROM lleva RIGHT JOIN (select nombre,dorsal FROM ciclista )c1 USING(dorsal)GROUP BY c1.nombre")->queryAll();

        return $this->render("ciclistas", [
                    "datosciclista" => $datosciclista,
                    "variable" => $variable,
                    "variablepuerto" => $variablepuerto,
                    "variablemaillot" => $variablemaillot,]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
