<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?=

ListView::widget([
    'dataProvider' => $eganadasequipo,
    'itemView' => '_eganadasequipo',
    'layout' => " \n {items} \n\n{pager}",
    'viewParams' => ["edadmedia" => $edadmedia,
        "nombredirector" => $nombredirector,
        "nomequipo" => $nomequipo,
        "rankingequipo" => $rankingequipo,
        "rankingequipopuerto" => $rankingequipopuerto,
        "puertosganados" => $puertosganados,
        "maillitsganados" => $maillitsganados,
        "rankingequipomaillots" => $rankingequipomaillots,
        "mejorciclistaequipo" => $mejorciclistaequipo,
        
    ],
]);
?>