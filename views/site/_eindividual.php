<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>


<div class="card alturaminima">
    <div class="card-body tarjeta">

        <h3 class="tituloEquipo"><?= Html::img('@web/img/' . $model->nomequipo . '.png', ['alt' => 'My logo']) ?></h3>
        <p>Número de ciclistas <?= $model->nciclistas ?></p>
        <p>
<?= Html::a('Ver ciclistas', ['site/ciclistasequipo', 'nomequipo' => $model->nomequipo], ['class' => 'btn btn-danger']) ?>

        </p>
    </div>
</div>

