<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use miloschuman\highcharts\Highcharts;

$this->title = 'ciclistas de equipo';
?>
<div class="row">
    <div class="list-view-ciclictas">

        
<h1 class="titulo-equipo2"> Equipo <?= Html::img('@web/img/' . $nomequipo . '.png', ['alt' => 'My logo']) ?> <?= Html::a('Estadisticas del equipo', ['site/estadisticaequipo', 'nomequipo' => $nomequipo], ['class' => 'btn btn-danger']) ?></h1>
<script src="https://code.highcharts.com/highcharts.js"></script>
<div class="graficas">
    <?php
    
    foreach($variable as $key=> $values){
                        $nomequipo1[] = ($values['nombre']);
                        $etapasGanadas1[] = intval($values['etapasGanadas']);
                        $puertosganados1[]=intval($variablepuerto[$key]['puertosganados']);
                        $maillotsganados1[]=intval($variablemaillot[$key]['maillotsllevados']);
                        
                    }
                    echo
                    Highcharts::widget([
                        'scripts' => ['modules'],
                        'options' => [
                            'chart' => ['type' => 'area'],
                            'title' => ['text' => 'Desempeño de los ciclistas'],
                            'xAxis' => ['categories' => $nomequipo1],
                            'yAxis' => ['title' => ['text' => 'Ganadas']],
                            'series' => [
                                [
                                   'name' => 'Etapas',
                                   'colorByPoint' => false,
                                   'data' => $etapasGanadas1,
                                ],
                                [
                                   'name' => 'Puertos',
                                   'colorByPoint' => false,
                                   'data' => $puertosganados1,
                                ],
                                [
                                   'name' => 'Maillots',
                                   'colorByPoint' => false,
                                   'data' => $maillotsganados1,
                                ],
                            ],
                        ],
                    ]);
    ?>
    </div>
        <?php
        echo ListView::widget([
            'dataProvider' => $resultado,
            'itemView' => '_ciclistaequipo',
            'layout' => " \n {items} \n\n{pager}",
            'itemOptions' => [
                'class' => 'tarjeta-ciclista-equipo',
            ],
            'viewParams' => [
    ],
        ]);
        ?>



    </div>
</div>