<?php

use yii\helpers\Html;

?>

<h1 class="titulo-equipo"> Equipo <?= Html::img('@web/img/' . $model->nomequipo . '.png', ['alt' => 'My logo']) ?> <?= Html::a('Estadisticas del equipo', ['site/estadisticaequipo', 'nomequipo' => $model->nomequipo], ['class' => 'btn btn-danger']) ?></h1><br>
<h3 class="nombre-ciclista">
    <br>Ciclista: <?= $model->nombre ?>, dorsal <?= $model->dorsal ?>, edad <?= $model->edad ?>

</h3>

<div class="row">
    <div class="col-sm-4" >
        <div class="card alturaminimacarac">
            <div class="card-body tarjeta-carac-ciclista">
                <h3 class="tituloEquipo">Etapas ganadas:<?= $etapasganadasciclista ?></h3>
                <p>
                <?PHP
                foreach ($datosetapa as $datos) {
                   echo "-Etapa: " . $datos->numetapa . ", Kms: " . $datos->kms . ", Salida: " . $datos->salida . ", Llegada: " . $datos->llegada . ".<br>";
                }
                ?>
</p>
            </div>
        </div>
    </div>
    <div class="col-sm-4" >
        <div class="card alturaminimacarac">
            <div class="card-body tarjeta-carac-ciclista">
                <h3 class="tituloEquipo">Puertos ganados:<?= $puertosganadasciclista ?></h3>
                <?PHP
                foreach ($datospuertos as $datos) {
                    print_r("-Nombre:" . $datos->nompuerto . ", Altura: " . $datos->altura . ", Pendiente: " . $datos->pendiente . "º.<br>");
                }
                ?>

            </div>
        </div>
    </div>
    <div class="col-sm-4" >
        <div class="card alturaminimacarac">
            <div class="card-body tarjeta-carac-ciclista">
                <h3 class="tituloEquipo">Maillots que ha llevado durante las etapas: <?= $maillotsllevados ?></h3>
                <?PHP
                foreach ($datosmaillot as $datos) {
                    print_r("-Etapa: Nº" . $datos->numetapa1 . ", Código: " . $datos->codigo1 . ", Color: " . $datos->color . ", Tipo: " . $datos->tipo . ", Premio: " . $datos->premio . "€.<br>");
                }
                ?>

            </div>
        </div>
    </div>
</div>
