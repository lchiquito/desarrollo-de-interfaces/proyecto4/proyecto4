<?php

use yii\helpers\Html;


?>
<div class="col-sm-12" >
    <div class="card alturaminimaciclicta">
        <div class="card-body tarjeta">
           
            <h3 class="tituloEquipo"><?= $model->nombre ?></h3>
             
            <p >Dorsal: <?= $model->dorsal ?>  <br> Edad: <?= $model->edad ?></p>
            <p>
                 <?= Html::a('Datos del ciclista', ['site/datosciclista','dorsal'=>$model->dorsal], ['class'=>'btn btn-danger'])?>
            </p>
        </div>
    </div>
</div>


