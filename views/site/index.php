<?php
use yii\bootstrap4\Html;
$this->title = 'Vuelta Ciclista 1994';
?>

<h1><br><br>¡La web oficial de la Vuelta Ciclista de 1994!</h1>
<style>
  h1{text-align: center;}
</style>

    <div id="carouselCiclistas" class="carousel slide" data-ride="carousel">     
        
        <ol class="carousel-indicators">
            <li data-target="#carouselCiclistas" data-slide-to="0" class="active"></li>
            <li data-target="#carouselCiclistas" data-slide-to="1" class="active"></li>
            <li data-target="#carouselCiclistas" data-slide-to="2" class="active"></li>
        </ol>
        
        
        
        <div class="carousel-inner">
            <div class="carousel-item active">
                <?= Html::img('@web/img/carrusel1.jpg',['class' => 'd-block w-60' ], ['alt' => 'My logo']) ?>
            </div>
            <div class="carousel-item">
                 <?= Html::img('@web/img/carrusel2.jpg',['class' => 'd-block w-60' ], ['alt' => 'My logo']) ?>
            </div>
            <div class="carousel-item">
                <?= Html::img('@web/img/carrusel3.jpg',['class' => 'd-block w-60' ], ['alt' => 'My logo']) ?>
            </div>
        </div>
        <a href="#carouselCiclistas" class="carousel-control-prev" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a href="#carouselCiclistas" class="carousel-control-next" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
<div class="card text-white bg-info mb-3 mt-5">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h5 class="card-title">Info card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
