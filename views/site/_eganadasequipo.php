<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

use yii\helpers\Html;

?>


<div class="body-content">
    <h1 class="titulo-equipo3">Equipo <?= Html::img('@web/img/' . $nomequipo . '.png', ['alt' => 'My logo']) ?> </h1>
   <h4 class="subtitulorankings">El director <?= $nombredirector ?> dirigía un equipo de <?= $edadmedia ?> años de edad media, con el que consiguió:</h4> <br>   
   <div class="row">
    <div class="col-sm-4" >
        <div class="card alturaminimacarac2">
            <div class="card-body tarjeta-carac-ciclista">
                <h3 class="tituloEquipo">Datos de etapas </h3>
                <p>Puesto en el ranking Nº: <?= $rankingequipo ?>.</p>
                <p>Etapas ganadas totales: <?= $model->eganadas ?>.</p>
                

            </div>
        </div>
    </div>
    <div class="col-sm-4" >
        <div class="card alturaminimacarac2">
            <div class="card-body tarjeta-carac-ciclista">
                <h3 class="tituloEquipo">Datos de puertos</h3>
               <p>Puesto en el ranking Nº: <?= $rankingequipopuerto ?>.</p>
                <p>Puertos ganados totales: <?= $puertosganados ?>.</p>

            </div>
        </div>
    </div>
    <div class="col-sm-4" >
        <div class="card alturaminimacarac2">
            <div class="card-body tarjeta-carac-ciclista">
                <h3 class="tituloEquipo">Datos de maillots </h3>
                <p>Puesto en el ranking Nº: <?= $rankingequipomaillots ?>.</p>
                <p>Maillots llevados totales: <?= $maillitsganados ?>.</p>

            </div>
        </div>
    </div>
</div>
   
    
</div>